from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.core import serializers
from .models import Book

# Create your views here.

def index(request):
    books = Book.objects.all().order_by("-count_like")[:5]
    return render(request, 'books/index.html', {"books": books})

def add(request):
    if request.method == "POST":
        book_id = request.POST["book_id"]
        try:
            book = Book.objects.get(book_id = book_id)
            book.count_like += 1
            book.save()
        except Exception as e:
            Book.objects.create(
                book_id = request.POST["book_id"],
                book_title = request.POST["book_title"],
                book_img = request.POST["book_img"],
                book_link = request.POST["book_link"],
                count_like = 1
            )
    return redirect("books:index")

def count_like(request, book_id):
    try:
        book = Book.objects.get(book_id = book_id)
        return JsonResponse(
            {"likes" : book.count_like}
        )
    except:
        return JsonResponse(
            {"likes" : 0}
        )    

def get_top5(request):
    books = Book.objects.all().order_by("-count_like")[:5]
    books_list = serializers.serialize('json', books)
    return HttpResponse(books_list, content_type="text/json-comment-filtered")

